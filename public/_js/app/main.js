/*----------------------------------------------------------------------------*\
        Place any project specific javascript here
\*----------------------------------------------------------------------------*/

'use strict';

var obj = obj || {};

var $j = jQuery.noConflict();
$j(function ($) {

    obj.config = {

        navigationMobile: function () {
            var $burgers = $('.js--mobile__icon');

            $burgers.click(function () {
                $(this).parent().next().find('nav').toggleClass('is-open');
            });
        },

        tabs: function () {

            $('.js--tabs-caption').on('click', 'li:not(.active)', function () {
                $(this).addClass('active').siblings().removeClass('active').closest('.js--tabs').find('.js--tabs--content').removeClass('active').eq($(this).index()).addClass('active');
            });
        },

        createListCheckbox: function () {
            var myItems = [],
                $myList = $('.js--list-checkbox');

            for (var i = 0; i < 100; i++) {
                myItems.push('<li><input id="checkbox' + i + '" type="checkbox" name="first"hidden /><label for="checkbox' + i + '">Опция' + i + '</label></li>');
            }

            $myList.append(myItems.join(''));
        },

        trackingCheckbox: function() {
            var FORM_ID = 'formCheckbox',
                MAX_CHECKED_COUNT = 3,
                checked = [],
                stored = localStorage.getItem(FORM_ID),
                $form = document.getElementById(FORM_ID);

            var list = Array.prototype.slice.call(document.querySelectorAll('input[type=checkbox]'));

            if(stored) {
                var _checked = JSON.parse(stored);

                _checked.forEach(function(index) {
                    list[index].checked = true;
                });

                checked = _checked;
            }

            $form.addEventListener('change', function(e) {
                var _index = list.indexOf(e.target);

                if(e.target.checked) {
                    if(checked.length === MAX_CHECKED_COUNT) {
                        list[checked[0]].checked = false;
                        checked.splice(0, 1);
                    }

                    checked.push(_index);
                } else {
                    checked.splice(checked.indexOf(_index), 1);
                }

                localStorage.setItem(FORM_ID, JSON.stringify(checked));
            });
        }
    };

    /*  Fire ALL the things
       \*----------------------------------------------------------------------------*/

    $(document).ready(function () {
        obj.config.navigationMobile();
        obj.config.tabs();
        obj.config.createListCheckbox();
        obj.config.trackingCheckbox();
    });
});